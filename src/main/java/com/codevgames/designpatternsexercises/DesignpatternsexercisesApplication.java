package com.codevgames.designpatternsexercises;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DesignpatternsexercisesApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesignpatternsexercisesApplication.class, args);

		System.out.println("App iniciadada");
	}

}
